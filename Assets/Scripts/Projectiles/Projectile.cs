using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	private Transform myTransform;
	private float _speed;
	private float _range;
	private int _dmg;
	
#region setters/getters
	public float Speed {
		get { return _speed; }
		set { _speed = value; }
	}
	
	public float Range {
		get { return _range; }
		set { _range = value; }
	}
	
	public int Damage {
		get { return _dmg; }
		set { _dmg = value; }
	}
#endregion
	
	void Awake() {
		myTransform = transform;
		_range = 30;
		_speed = 10;
		_dmg = 10;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		myTransform.position += myTransform.forward * _speed * Time.deltaTime;
		_range -= _speed * Time.deltaTime;
		if (_range <= 0)
			Die();
	}
	
	private void Die() {
		Destroy(gameObject);	
	}
}
