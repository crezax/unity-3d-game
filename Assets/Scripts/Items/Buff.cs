using UnityEngine;
using System.Collections.Generic;

public class Buff {
	private Dictionary<AttributeName, int> _attributes;
	private Dictionary<RatingName, int> _skills;
	private Dictionary<VitalName, int> _vitals;
	private List<KeyValuePair<string, int>> _stats;
	
	public void AddStat(AttributeName an, int v) {
		_attributes.Add(an, v);
		_stats = null;
	}
	
	public void AddStat(RatingName sn, int v) {
		_skills.Add(sn, v);
		_stats = null;
	}
	
	public void AddStat(VitalName vn, int v) {
		_vitals.Add(vn, v);
		_stats = null;
	}
	
	public List<KeyValuePair<string, int>> GetStats() {
		if (_stats == null) GenStats();
		return _stats;
	}
	
	public void Apply(BaseCharacter bc) {
		foreach (KeyValuePair<AttributeName, int> p in _attributes) {
			bc.GetPrimaryAttribute((int)p.Key).BuffValue += p.Value;
		}
		foreach (KeyValuePair<VitalName, int> p in _vitals) {
			bc.GetVital((int)p.Key).BuffValue += p.Value;
		}
		foreach (KeyValuePair<RatingName, int> p in _skills) {
			bc.GetRating((int)p.Key).BuffValue += p.Value;
		}
	}
			
	private void GenStats() {
		_stats = new List<KeyValuePair<string, int>>();
		foreach (KeyValuePair<AttributeName, int> p in _attributes) {
			_stats.Add(new KeyValuePair<string, int>(p.Key.ToString(), p.Value));
		}
		foreach (KeyValuePair<VitalName, int> p in _vitals) {
			_stats.Add(new KeyValuePair<string, int>(p.Key.ToString(), p.Value));
		}
		foreach (KeyValuePair<RatingName, int> p in _skills) {
			_stats.Add(new KeyValuePair<string, int>(p.Key.ToString(), p.Value));
		}
	}
}
