using UnityEngine;
using System.Collections;

public abstract class ConsumableItem : Item, IUsableItem {
	public abstract int Cooldown();
	
	public void Use(BaseCharacter bc) {
		StackSize -= 1;
		OnUse(bc);
	}
	
	protected abstract void OnUse(BaseCharacter bc);
}
