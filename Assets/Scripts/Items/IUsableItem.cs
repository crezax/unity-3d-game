using UnityEngine;
using System.Collections;

public interface IUsableItem {
	int Cooldown();
	void Use(BaseCharacter bc);
}
