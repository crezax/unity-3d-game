using UnityEngine;
using System;
using System.Collections;

public class Interval<T> where T : IComparable {
	private T _minValue;
	private T _curValue;
	private T _maxValue;
	
	public Interval(T min, T cur, T max) {
		_minValue = min;
		_curValue = cur;
		_maxValue = max;
	}
	
	public T Min {
		get {
			return _minValue;
		}
		set {
			_minValue = value;
			if (_minValue.CompareTo(_maxValue) > 0)
				_maxValue = _minValue;
		}
	}
	
	public T Cur {
		get {
			return _curValue;	
		}
		set {
			_curValue = value;
			if (_curValue.CompareTo(_minValue) < 0)
				_curValue = _minValue;
			if (_curValue.CompareTo(_maxValue) > 0)
				_curValue = _maxValue;
		}
	}
	
	public T Max {
		get {
			return _maxValue;	
		}
		set {
			_maxValue = value;
			if (_minValue.CompareTo(_maxValue) > 0)
				_minValue = _maxValue;
		}
	}
}
