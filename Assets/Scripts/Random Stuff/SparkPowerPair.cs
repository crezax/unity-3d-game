using UnityEngine;
using System.Collections;

public class SparkPowerPair {
	public Spark spark;
	public int power;
	
	public SparkPowerPair(Spark spark, int power) {
		this.spark = spark;
		this.power = power;
	}
}
