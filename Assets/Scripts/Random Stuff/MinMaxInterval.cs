using System;

public class MinMaxInterval<T> where T : IComparable {
	private T _minValue;
	private T _maxValue;
	
	public MinMaxInterval(T min, T max) {
		_minValue = min;
		_maxValue = max;
	}
	
	public T Min {
		get {
			return _minValue;
		}
		set {
			_minValue = value;
			if (_minValue.CompareTo(_maxValue) > 0)
				_maxValue = _minValue;
		}
	}
	
	public T Max {
		get {
			return _maxValue;	
		}
		set {
			_maxValue = value;
			if (_minValue.CompareTo(_maxValue) > 0)
				_minValue = _maxValue;
		}
	}
}
