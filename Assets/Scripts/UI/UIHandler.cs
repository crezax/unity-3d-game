using UnityEngine;
using System.Collections;

public class UIHandler : MonoBehaviour {
	public const string PLAYER_HEALTH_ENERGY_BAR = "Player HealthEnergyBar";
	public const string TARGET_HEALTH_ENERGY_BAR = "Target HealthEnergyBar";
	
	public VitalBarHandler _playerHealthEnergyBar;
	private Rect _playerHealthEnergyBarRect;
	
	public VitalBarHandler _targetHealthEnergyBar;
	private Rect _targetHealthEnergyBarRect;
	
	#region setters/getters
	public Rect PlayerHealthEnergyBarPixelInset {
		get { return _playerHealthEnergyBarRect; }
		set { 
			_playerHealthEnergyBarRect = value;
			_playerHealthEnergyBar.SetPosition(value);
		}
	}
	
	public Rect TargetHealthEnergyBarPixelInset {
		get { return _playerHealthEnergyBarRect; }
		set { 
			_targetHealthEnergyBarRect = value;
			_targetHealthEnergyBar.SetPosition(value);
		}
	}
	#endregion
	
	void Awake() {
		_playerHealthEnergyBar = transform.FindChild(PLAYER_HEALTH_ENERGY_BAR).GetComponent<VitalBarHandler>();
		_targetHealthEnergyBar = transform.FindChild(TARGET_HEALTH_ENERGY_BAR).GetComponent<VitalBarHandler>();
		
		Debug.Log("UI HANDLER AWAKENED");
	}
	// Use this for initialization
	void Start () {
		ReloadUI ();
		Debug.Log ("UI HANDLER STARTED");
		//_playerHealthEnergyBar = transform.FindChild(PLAYER_HEALTH_ENERGY_BAR).GetComponent<VitalBarHandler>();
	}
	
	// Update is called once per frame
	public void Update () {

	}

	public void ReloadUI () {
		InitPlayerHealthEnergyBar();
		InitTargetHealthEnergyBar();
	}
	
	public void AssignPlayer(BaseCharacter character) {
		_playerHealthEnergyBar.Target = character;	
	}
	
	public void AssignTarget(BaseCharacter character) {
		_targetHealthEnergyBar.Target = character;
	}
	
	//TODO: Make it work nicely, like add a queue of texts or sth
	public static void ShowText(string text, float t = 5) {
		int tries = 0;
		while (GameObject.Find ("CenterText" + tries) != null) tries++;
		GameObject tmp = new GameObject("CenterText" + tries);
		tmp.AddComponent(typeof(GUIText));
		GUIText gt = tmp.guiText;
		gt.anchor = TextAnchor.MiddleCenter;
		gt.fontSize = 20;
		gt.material.color = Color.red;
		gt.text = text;
		gt.transform.position = new Vector3(0.5f, 0.7f - tries * 0.03f, 0.5f);
		Destroy (tmp, t);
	}
	
	private void InitPlayerHealthEnergyBar() {
		PlayerHealthEnergyBarPixelInset = new Rect(Screen.width / 20, Screen.height * 9 / 10, Screen.width / 4, Screen.height / 20);
	}

	private void InitTargetHealthEnergyBar () {
		TargetHealthEnergyBarPixelInset = new Rect(Screen.width - Screen.width / 20 - Screen.width / 4, Screen.height * 9 / 10, Screen.width / 4, Screen.height / 20);
	}
}
