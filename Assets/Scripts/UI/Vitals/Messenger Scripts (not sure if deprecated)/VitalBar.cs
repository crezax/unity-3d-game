using UnityEngine;
using System.Collections;

public abstract class VitalBar : MonoBehaviour {
	private float _maxWidth;
	private GUITexture _vitalBar;
	// Use this for initialization
	void Start () {
		_maxWidth = guiTexture.pixelInset.width;
		_vitalBar = GetComponent<GUITexture>();
		
		OnEnable();
	}
	
	public void OnValueChanged(int curValue, int maxValue) {
		_vitalBar.pixelInset = new Rect(guiTexture.pixelInset.x,
										guiTexture.pixelInset.y,
										(float)curValue / (float)maxValue * _maxWidth,
										guiTexture.pixelInset.height
								);
	}
	
	public abstract void OnEnable();
	public abstract void OnDisable();
}
