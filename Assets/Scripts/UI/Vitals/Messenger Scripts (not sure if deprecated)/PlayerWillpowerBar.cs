using UnityEngine;
using System.Collections;

public class PlayerWillpowerBar : VitalBar {
	public const string MESSAGE = "Player Willpower Changed";
	
	public override void OnEnable() {
		Messenger<int, int>.AddListener(MESSAGE, OnValueChanged);
	}
	
	public override void OnDisable() {
		Messenger<int, int>.RemoveListener(MESSAGE, OnValueChanged);
	}
}
