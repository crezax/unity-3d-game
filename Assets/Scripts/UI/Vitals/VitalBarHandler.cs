/// <summary>
/// This script presents new approach to UI bars handling.
/// Unlike the VitalBar script and its subclasses, it doesn't use C# Messenger Extended and gives
/// us a chance to handle vital bars in multiplayer game.
/// Also, this script handles any vital bar that is child object of game object script is attached to.
/// All it requires is GUITexture named the same as vital name it is supposed to handle.
/// </summary>
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class VitalBarHandler : MonoBehaviour {
	private float _maxWidth;
	private Dictionary<VitalName, GUITexture> _vitalBars;
	private GUITexture _background;
	private BaseCharacter _target;
	
	void Awake() {
		Debug.Log ("AWAKE");
		_vitalBars = new Dictionary<VitalName, GUITexture>();
		_background = transform.FindChild("Background").GetComponent<GUITexture>();
		_maxWidth = _background.pixelInset.width;
		foreach (VitalName type in Enum.GetValues(typeof(VitalName)))
			if (transform.FindChild(type.ToString()) != null)
				_vitalBars.Add(type, transform.FindChild(type.ToString()).GetComponent<GUITexture>());
		SetEnabled(false);
		Debug.Log("VITAL BAR HANDLER AWAKENED");
	}
	// Use this for initialization
	void Start () {
		
	}
	
	public void Update() {
		foreach (VitalName type in _vitalBars.Keys) {
			//Debug.Log((type).ToString() + " - Base Value " + _target.GetVital((int)type).AdjustedBaseValue.ToString());
			//Debug.Log((type).ToString() + " - Current Value " + _target.GetVital((int)type).CurValue.ToString());
			int curValue = _target.GetVital((int)type).CurValue;
			int maxValue = _target.GetVital((int)type).AdjustedBaseValue;
			_vitalBars[type].pixelInset = new Rect(_vitalBars[type].pixelInset.x,
											_vitalBars[type].pixelInset.y,
											(float)curValue / (float)maxValue * _maxWidth,
											_vitalBars[type].pixelInset.height
										);
		}
	}
	
	public BaseCharacter Target {
		get { return _target; }
		set { 
			SetEnabled(value != null);
			_target = value;
//			Debug.Log("ASSIGNING TARGET " + value.ToString() + " " + (_target != null));
		}
	}
	
	public void SetPosition(Rect inset) {
		foreach(GUITexture vitalBar in _vitalBars.Values) {
			vitalBar.pixelInset = inset;	
		}
		_background.pixelInset = inset;
		_maxWidth = inset.width;
	}
	
	private void SetEnabled(bool enabled) {
		Debug.Log (this.ToString() + " SET ENABLED " + enabled.ToString());
		this.enabled = enabled;
		foreach(GUITexture vitalBar in _vitalBars.Values) {
			vitalBar.enabled = enabled;	
		}
		_background.enabled = enabled;
	}
}
