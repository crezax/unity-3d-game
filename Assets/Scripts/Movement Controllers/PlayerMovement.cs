using UnityEngine;
using System.Collections;

public class PlayerMovement : FPSInputController {
	private GameObject model;
	private PlayerCharacter _pc;
	private CharacterMotor _cm;
	private Attack _attack;
	private Targetting _targetting;
	private Transform _transform;
	// Use this for initialization
	void Start () {
		model = GameObject.Find("Player Character Model");
		_pc = GetComponent<PlayerCharacter>();
		_cm = GetComponent<CharacterMotor>();
		_targetting = GetComponent<Targetting>();
		_transform = transform;
		_attack = new Melee(_pc.MainHand);
	}
	
	// Update is called once per frame
	new void Update () {
		
		if (_pc.IsAlive() == false) {
			enabled = false;
			model.animation.Play("die");
			_cm.enabled = false;
			foreach (MouseLook ml in GetComponents<MouseLook>())
				ml.enabled = false;
			return;
		}
		
		base.Update();
		
		if (Input.GetKey(KeyCode.A) ||
			Input.GetKey(KeyCode.S) ||
			Input.GetKey(KeyCode.D) ||
			Input.GetKey(KeyCode.W) ||
			Input.GetKey(KeyCode.UpArrow) ||
			Input.GetKey(KeyCode.RightArrow) ||
			Input.GetKey(KeyCode.LeftArrow) ||
			Input.GetKey(KeyCode.DownArrow)
			) {
			model.animation.CrossFade("spark");
		}
		
		if (Input.GetKeyUp(KeyCode.A) ||
			Input.GetKeyUp(KeyCode.S) ||
			Input.GetKeyUp(KeyCode.D) ||
			Input.GetKeyUp(KeyCode.W) ||
			Input.GetKeyUp(KeyCode.UpArrow) ||
			Input.GetKeyUp(KeyCode.RightArrow) ||
			Input.GetKeyUp(KeyCode.LeftArrow) ||
			Input.GetKeyUp(KeyCode.DownArrow)
			) {
			model.animation.CrossFade("idle");	
		}
		
		if (Input.GetKey(KeyCode.F)) {
			if (_pc.MainHand.Range > Vector3.Distance(_targetting.SelectedTarget.transform.position, _transform.position) && _attack.IsReady()) {
				_attack.Perform(gameObject, _targetting.SelectedTarget);
			}
			model.animation.CrossFade("melee");
			model.animation.CrossFadeQueued("steady");
			model.animation.CrossFadeQueued("idle");
		}
	}
}
