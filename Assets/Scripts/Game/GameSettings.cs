using UnityEngine;
using System;
using System.Collections;

public class GameSettings : MonoBehaviour {
	public const string PLAYER_SPAWN_POINT = "Player Spawn Point";	// name of spawn point game object
	
	void Awake() {
		DontDestroyOnLoad(this);	
	}
	
#region Saving/Loading
	public void SaveCharacterData() {
		PlayerCharacter pcScript = GameObject.Find("Player Character").GetComponent<PlayerCharacter>();
		
		PlayerPrefs.SetString("Player Name", pcScript.Name);
		
		SaveAttributes (pcScript);
		
		SaveVitals (pcScript);
		
		SaveSkills (pcScript);
		
		PlayerPrefs.Save();
	}

	static void SaveAttributes (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++)
			PlayerPrefs.SetInt(((AttributeName)i).ToString() + " - Base Value", pcScript.GetPrimaryAttribute(i).BaseValue);
	}

	static void SaveVitals (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++) {
			PlayerPrefs.SetInt(((VitalName)i).ToString() + " - Base Value", pcScript.GetVital(i).BaseValue);
			PlayerPrefs.SetInt(((VitalName)i).ToString() + " - Current Value", pcScript.GetVital(i).CurValue);
		}
	}

	static void SaveSkills (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(RatingName)).Length; i++) {
			PlayerPrefs.SetInt(((RatingName)i).ToString() + " - Base Value", pcScript.GetRating(i).BaseValue);
		}
	}
	
	public void LoadCharacterData() {
		PlayerCharacter pcScript = GameObject.Find("Player Character").GetComponent<PlayerCharacter>();
		
		pcScript.Name = PlayerPrefs.GetString("Player Name", "Name me");
		
		LoadAttributes(pcScript);
		
		LoadVitals (pcScript);
		
		LoadSkills (pcScript);
	}

	static void LoadAttributes (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++) {
			pcScript.GetPrimaryAttribute(i).BaseValue = PlayerPrefs.GetInt(((AttributeName)i).ToString() + " - Base Value", 0);
		}
	}

	static void LoadVitals (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++) {
			pcScript.GetVital(i).BaseValue = PlayerPrefs.GetInt(((VitalName)i).ToString() + " - Base Value", 0);
			
			pcScript.GetVital(i).Update();
			pcScript.GetVital(i).CurValue = PlayerPrefs.GetInt(((VitalName)i).ToString() + " - Current Value", 1);
			Debug.Log(((VitalName)i).ToString() + " - Base Value " + pcScript.GetVital(i).AdjustedBaseValue.ToString());
			Debug.Log(((VitalName)i).ToString() + " - Current Value " + pcScript.GetVital(i).CurValue.ToString());
			Debug.Log(((VitalName)i).ToString() + " - Mods " + pcScript.GetVital(i).GetModifyingAttributesString());
		}
	}

	static void LoadSkills (PlayerCharacter pcScript)
	{
		for (int i = 0; i < Enum.GetValues(typeof(RatingName)).Length; i++) {
			pcScript.GetRating(i).BaseValue = PlayerPrefs.GetInt(((RatingName)i).ToString() + " - Base Value", 0);
			Debug.Log(((RatingName)i).ToString() + " - Base Value " + pcScript.GetRating(i).BaseValue.ToString());
			Debug.Log(((RatingName)i).ToString() + " - Mods " + pcScript.GetRating(i).GetModifyingAttributesString());
		}
	}
#endregion
}
