using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public Transform target;
	
	private Interval<float> _distance;
	private Transform _transform;
	private float _xAngle;
	private float _yAngle;
	private float _xSpeed;
	private float _ySpeed;
	
	private Quaternion _rotation;
	
	private const float BASE_X_ANGLE = 0;
	private const float BASE_Y_ANGLE = 20;
	
	void Awake() {
		_transform = transform;
		_distance = new Interval<float>(4,4,8);
		_xSpeed = 5.0f;
		_ySpeed = 2.4f;
		_xAngle = BASE_X_ANGLE;
		_yAngle = BASE_Y_ANGLE;
		_transform.rotation = Quaternion.Euler(BASE_Y_ANGLE, BASE_X_ANGLE, 0);	
	}
	
	// Use this for initialization
	void Start () {
		if (target == null) {
			Debug.LogError("Camera doesn't have a target");
			enabled = false;
		}
	}
	
	void LateUpdate() {
		if (Input.GetButton("Rotate Camera")) {
			_transform.rotation = Quaternion.Euler(
				_transform.eulerAngles.x - Input.GetAxis("Mouse Y") * _ySpeed,
				_transform.eulerAngles.y + Input.GetAxis("Mouse X") * _xSpeed, 
				0
			);
			if (Input.GetButton("Mouse Move")) {
				transform.root.rotation = Quaternion.Euler(transform.root.rotation.eulerAngles.x, _transform.rotation.eulerAngles.y, transform.root.rotation.eulerAngles.z);
				_transform.rotation = target.rotation * Quaternion.Euler(new Vector3(BASE_Y_ANGLE, BASE_X_ANGLE, 0));
			}
		}
		else {
			_transform.rotation = Quaternion.Lerp(
				_transform.rotation,
				target.rotation * Quaternion.Euler(new Vector3(BASE_Y_ANGLE, BASE_X_ANGLE, 0)),
				Time.deltaTime
			);
		}
		
		_distance.Cur -= Input.GetAxis("Mouse ScrollWheel");
		
		_transform.position = _transform.rotation * new Vector3(0, 0, -_distance.Cur) + target.position;
	}
}
