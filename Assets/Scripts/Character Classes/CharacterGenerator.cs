using UnityEngine;
using System;
using System.Collections;

public class CharacterGenerator : MonoBehaviour {
	private PlayerCharacter _char;
	public GameObject playerPrefab; // TODO: Make it private after testing?
	private int pointsLeft;
	private const int STARTING_POINTS = 150;
	private const int MIN_STARTING_ATT_VALUE = 10;
	private const int STARTING_ATT_VALUE = 40;
#region GUI constants
	private const int OFFSET = 5;
	private const int LINE_HEIGHT = 50;
	
	private const int STAT_LABEL_WIDTH = 200;
	private const int VALUE_LABEL_WIDTH = 60;
	
	private const int BUTTON_WIDTH = 40;
	private const int BUTTON_HEIGHT = 40;
	
	private const int STAT_STARTING_POS = 80;
	
	private int margin;
	
	private int calculateMargin() {
		return (Screen.width - 
				STAT_LABEL_WIDTH * 2 -
				VALUE_LABEL_WIDTH * 2 -
				BUTTON_WIDTH * 2 -
				OFFSET * 4) / 2;	
	}
#endregion
	
#region styles
	public GUISkin basicCreationSkin;
	public GUIStyle valueStyle;
	public Texture backgroundTexture;
#endregion
	
	// Use this for initialization
	void Start () {
		GameObject pc = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		pc.name = "Player Character";
		
		_char = pc.GetComponent<PlayerCharacter>();
		
		pointsLeft = STARTING_POINTS;
		for (int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++) {
			_char.GetPrimaryAttribute(i).BaseValue = MIN_STARTING_ATT_VALUE;
			while (_char.GetPrimaryAttribute(i).BaseValue != STARTING_ATT_VALUE) {
				pointsLeft -= _char.GetPrimaryAttribute(i).LevelUp();
			}
		}
		_char.StatUpdate();
		CreateCharacter(); //REMOVE THIS to enable character modifications
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	/*void OnGUI() {
		GUI.skin = basicCreationSkin;
		margin = calculateMargin();
		
		//DisplayBackground();
		DisplayName();
		DisplayPointsLeft();
		DisplayAttributes();
		DisplayVitals();
		DisplaySkills();
		if (_char.Name == string.Empty) GUI.enabled = false;
		DisplayCreateButton();
		GUI.enabled = true;
	}*/
	
	private void DisplayBackground() {
		backgroundTexture.wrapMode = TextureWrapMode.Clamp;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);	
	}
	
	private void DisplayName() {
		GUI.Label(new Rect(margin + OFFSET, OFFSET, 50, 25), "Name:");
		_char.Name = GUI.TextField(new Rect(margin + 65, OFFSET, 100, 25), _char.Name);
	}
	
	private void DisplayAttributes() {
		for (int i = 0; i < Enum.GetValues(typeof(AttributeName)).Length; i++) {
			GUI.Label(new Rect( margin + OFFSET,
								STAT_STARTING_POS + i * LINE_HEIGHT,
								STAT_LABEL_WIDTH,
								LINE_HEIGHT
					), ((AttributeName)i).ToString());	
			
			GUI.Label(new Rect(	margin + OFFSET + STAT_LABEL_WIDTH,
								STAT_STARTING_POS + i * LINE_HEIGHT,
								VALUE_LABEL_WIDTH,
								LINE_HEIGHT
					), _char.GetPrimaryAttribute(i).AdjustedBaseValue.ToString(), valueStyle);	
			
			if (GUI.Button(new Rect(margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH,
									STAT_STARTING_POS + i * LINE_HEIGHT,
									BUTTON_WIDTH,
									BUTTON_HEIGHT
							), "+")) {
				
				if (pointsLeft >= _char.GetPrimaryAttribute(i).ExpToLevel) {
					pointsLeft -= _char.GetPrimaryAttribute(i).LevelUp();
					_char.StatUpdate();
				}
				
			}
			
			if (GUI.Button(new Rect(margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH + BUTTON_WIDTH,
									STAT_STARTING_POS + i * LINE_HEIGHT,
									BUTTON_WIDTH,
									BUTTON_HEIGHT
							), "-")) {
				
				if (_char.GetPrimaryAttribute(i).BaseValue > MIN_STARTING_ATT_VALUE) {
					pointsLeft += _char.GetPrimaryAttribute(i).LevelDown();
					_char.StatUpdate();
				}
				
			}
		}
	}
	
	private void DisplayVitals() {
		int y = STAT_STARTING_POS + (Enum.GetValues (typeof(AttributeName)).Length + 1) * LINE_HEIGHT;
		for (int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++) {
			GUI.Label(new Rect(	margin + OFFSET,
								y + i * LINE_HEIGHT,
								STAT_LABEL_WIDTH,
								LINE_HEIGHT
					), ((VitalName)i).ToString());	
			
			GUI.Label(new Rect(	margin + OFFSET + STAT_LABEL_WIDTH,
								y + i * LINE_HEIGHT,
								VALUE_LABEL_WIDTH,
								LINE_HEIGHT
					), _char.GetVital(i).AdjustedBaseValue.ToString(), valueStyle);
		}
	}
	
	private void DisplaySkills() {
		for (int i = 0; i < Enum.GetValues(typeof(RatingName)).Length; i++) {
			GUI.Label(new Rect(	margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH + 2 * BUTTON_WIDTH + OFFSET * 3,
								STAT_STARTING_POS + i * LINE_HEIGHT,
								STAT_LABEL_WIDTH,
								LINE_HEIGHT
					), ((RatingName)i).ToString());
			
			GUI.Label(new Rect(	margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH + 2 * BUTTON_WIDTH + OFFSET * 3 + STAT_LABEL_WIDTH,
								STAT_STARTING_POS + i * LINE_HEIGHT, 
								VALUE_LABEL_WIDTH, 
								LINE_HEIGHT
					), _char.GetRating(i).AdjustedBaseValue.ToString(), valueStyle);	
		}
	}
	
	private void DisplayPointsLeft() {
		GUI.Label(new Rect(	margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH + 2 * BUTTON_WIDTH + OFFSET * 3,
							10, 
							150, 
							25
				), "Points left: " + pointsLeft.ToString());	
	}
	
	private void DisplayCreateButton() {
		int y = STAT_STARTING_POS + (Enum.GetValues(typeof(AttributeName)).Length + 1) * LINE_HEIGHT;
		int x = margin + OFFSET + STAT_LABEL_WIDTH + VALUE_LABEL_WIDTH + 2 * BUTTON_WIDTH + OFFSET * 3;
		
		GUIStyle buttonStyle = GUI.skin.button;
		buttonStyle.fontSize = 20;
		buttonStyle.fontStyle = FontStyle.Bold;
		
		if (GUI.Button(new Rect(x, y, 100, LINE_HEIGHT), "Create", buttonStyle)) {
			CreateCharacter ();
		}
	}
	
	private void UpdateCurrentVitalValues() {
		for (int i = 0; i < Enum.GetValues(typeof(VitalName)).Length; i++)
			_char.GetVital(i).CurValue = _char.GetVital(i).AdjustedBaseValue;	
	}

	void CreateCharacter ()
	{
		GameSettings gsScript = GameObject.Find("_GameSettings").GetComponent<GameSettings>();
		
		UpdateCurrentVitalValues();
		
		gsScript.SaveCharacterData();
		
		Application.LoadLevel("Level1");
	}
}