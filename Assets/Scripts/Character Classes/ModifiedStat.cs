using System.Collections.Generic;					// For List<>

public class ModifiedStat : BaseStat {
	private List<ModifyingAttribute> _mods;
	private int _modValue;
	
	public ModifiedStat() {
		_mods = new List<ModifyingAttribute>();
		_modValue = 0;
	}
	
	public void AddModifier(ModifyingAttribute mod) {
		_mods.Add(mod);
	}
	
	public new int AdjustedBaseValue {
		get { return BaseValue + BuffValue + _modValue; }
	}
	
	private void CalculateModValue() {
		_modValue = 0;
		
		foreach (ModifyingAttribute att in _mods)
			_modValue += (int)(att.attribute.AdjustedBaseValue * att.ratio);
	}
	
	public void Update() {
		CalculateModValue();	
	}
	
	public string GetModifyingAttributesString() {
		string tmp = string.Empty;
		
		for (int i = 0; i < _mods.Count; i++) {
			tmp += _mods[i].attribute.Name;
			tmp += "_";
			tmp += _mods[i].ratio;
			
			if (i < _mods.Count - 1)
				tmp += "|";
		}
		
		return tmp;
	}
}

public struct ModifyingAttribute {
	public Attribute attribute;
	public float ratio;
	
	public ModifyingAttribute(Attribute attribute, float ratio) {
		this.attribute = attribute;
		this.ratio = ratio;
	}
}