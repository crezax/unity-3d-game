public class Vital : ModifiedStat {
	private int _curValue;
	
	public Vital() {
		_curValue = 0;	
		StartingExpToLevel = 50;
		LevelModifier = 1.1f;
	}
	
	public int CurValue {
		get { 
			if (_curValue > AdjustedBaseValue)
				_curValue = AdjustedBaseValue;
			return _curValue; 
		}
		set { 
			_curValue = value; 
			_curValue = System.Math.Min(_curValue, AdjustedBaseValue);
			_curValue = System.Math.Max(_curValue, 0);
		}
	}
	
	public void Take(int amount) {
		_curValue -= amount;	
	}
}

public enum VitalName {
	Health,
	Energy,
	Willpower,
	Mana
}