using UnityEngine;
using System;
using System.Collections;

public abstract class BaseCharacter : MonoBehaviour {
	public GameObject hitSpot;
	
	private string _name;
	private int _level;
	private int _exp;
	
	private Attribute[] _primaryAttribute;
	private Vital[] _vital;
	private Rating[] _skill;
	
	public void Awake() {
		Debug.Log ("AWAKING BASE CHARACTER");
		_name = string.Empty;
		_level = 1;
		_exp = 0;
		
		_primaryAttribute = new Attribute[
			Enum.GetValues(typeof(AttributeName)).Length
		];
		_vital = new Vital[
			Enum.GetValues(typeof(VitalName)).Length
		];
		_skill = new Rating[
			Enum.GetValues(typeof(RatingName)).Length
		];
		
		SetupPrimaryAttributes();
		SetupVitals();
		SetupRatings();
		
		StatUpdate();
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
#region setters/getters
	public string Name {
		get { return _name; }
		set { _name = value; }
	}
	
	public int Level {
		get { return _level; }
		set { _level = value; }
	}
	
	public int Exp {
		get { return _exp; }
		set { _exp = value; }
	}
#endregion

	public void AddExp(int exp) {
		this._exp += exp;
		
		CalculateLevel();
	}
	
	private void CalculateLevel() {
			
	}
	
	private void SetupPrimaryAttributes() {
		for (int i = 0; i < _primaryAttribute.Length; i++) {
			_primaryAttribute[i] = new Attribute(((AttributeName)i).ToString());	
		}
	}
	
	private void SetupVitals() {
		for (int i = 0; i < _vital.Length; i++) {
			_vital[i] = new Vital();	
		}
		
		SetupVitalModifiers();
	}
	
	private void SetupRatings() {
		for (int i = 0; i < _skill.Length; i++) {
			_skill[i] = new Rating();	
		}
		
		SetupRatingModifiers();
	}
	
	public Attribute GetPrimaryAttribute(int index) {
		return _primaryAttribute[index];	
	}
	
	public Vital GetVital(int index) {
		return _vital[index];	
	}
	
	public Rating GetRating(int index) {
		return _skill[index];	
	}
	
	/// <summary>
	/// Setups the vital modifiers.
	/// One should override this function, in case one wants the character to gain vitals from other stats,
	/// for example, one can create character, that gains tons of health from Concentration.
	/// </summary>
	protected virtual void SetupVitalModifiers() {
		// health		
		GetVital((int)VitalName.Health).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Constitution), 0.5f));
		// energy
		GetVital((int)VitalName.Energy).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Constitution), 1f));
		// willpower
		GetVital((int)VitalName.Willpower).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration), 0.5f));
		// mana
		GetVital((int)VitalName.Mana).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration), 1f));
	}
	
	/// <summary>
	/// Setups the rating modifiers.
	/// One should override this function, to create a character, that benefits from different attributes.
	/// </summary>
	protected virtual void SetupRatingModifiers() {
		//Crushing Defense Rating
		GetRating((int)RatingName.CrushingDefenseRating).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Strength), 0.1f));
		//Piercing Defense Rating
		GetRating((int)RatingName.PiercingDefenseRating).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Dexterity), 0.1f));
		//Slashing Defense Rating
		GetRating((int)RatingName.SlashingDefenseRating).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Strength), 0.05f));
		GetRating((int)RatingName.SlashingDefenseRating).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Dexterity), 0.05f));
	}
	
	public void StatUpdate() {
		foreach (Vital vital in _vital)
			vital.Update();
		foreach (Rating skill in _skill)
			skill.Update();
	}
	
	public virtual bool IsAlive() {
		return GetVital((int)VitalName.Health).CurValue > 0;	
	}

	public int ComputeDamageTaken (Damage damage)
	{
		int def;
		switch (damage.DmgType) {
		case DamageType.Crushing:
			def = GetRating((int)RatingName.CrushingDefenseRating).AdjustedBaseValue;
			break;
		case DamageType.Fire:
			def = GetRating((int)RatingName.FireResistance).AdjustedBaseValue;
			break;
		case DamageType.Frost:
			def = GetRating((int)RatingName.FrostResistance).AdjustedBaseValue;
			break;
		case DamageType.ManaBurn:
			def = GetRating((int)RatingName.MagicalProtectionRating).AdjustedBaseValue;
			break;
		case DamageType.Piercing:
			def = GetRating((int)RatingName.PiercingDefenseRating).AdjustedBaseValue;
			break;
		case DamageType.Slashing:
			def = GetRating((int)RatingName.SlashingDefenseRating).AdjustedBaseValue;
			break;
		case DamageType.Spiritual:
			def = GetRating((int)RatingName.ForceOfWillRating).AdjustedBaseValue;
			break;
		case DamageType.Weakening:
			def = GetRating((int)RatingName.EnduranceRating).AdjustedBaseValue;
			break;
		case DamageType.TrueDamage:
		case DamageType.TrueManaBurn:
		case DamageType.TrueSpiritual:
		case DamageType.TrueWeaken:
		default:
			def = 0;
			break;
		}
		
		int val = damage.DamageValue();
		Debug.Log (1.0f - ((float)def / ((float)def + 100.0f)) + " " + val);
		return Math.Max((int)((1.0f - ((float)def / ((float)def + 100.0f))) * (float)val), 1);
	}
	
	public void ApplyDamage(Damage damage) {
		int dmg = ComputeDamageTaken(damage);
		Debug.Log (dmg);
		
		switch (damage.DmgType) {
		case DamageType.Crushing:
		case DamageType.Piercing:
		case DamageType.Slashing:
		case DamageType.Fire:
		case DamageType.Frost:
		case DamageType.TrueDamage:
			GetVital((int)VitalName.Health).CurValue -= dmg;
			break;
		case DamageType.Weakening:
		case DamageType.TrueWeaken:
			GetVital((int)VitalName.Energy).CurValue -= dmg;
			break;
		case DamageType.ManaBurn:
		case DamageType.TrueManaBurn:
			GetVital((int)VitalName.Mana).CurValue -= dmg;
			break;
		case DamageType.Spiritual:
		case DamageType.TrueSpiritual:
			GetVital((int)VitalName.Willpower).CurValue -= dmg;
			break;
		}
	}
	
	//public abstract void DisplayHealth();
}
