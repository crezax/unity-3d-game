using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class SparkUser : BaseCharacter {
	public GameObject spellSpawnPoint;
	
	private List<Spark> sparks;
	
	public ReadOnlyCollection<Spark> Sparks {
		get {
			return new ReadOnlyCollection<Spark>(sparks);	
		}
	}
	
	public void AddSpark(Spark spark) {
		sparks.Add(spark);
		sparks.Sort();
	}
	
	public void RemoveSpark(Spark spark) {
		sparks.Remove(spark);
	}
}
