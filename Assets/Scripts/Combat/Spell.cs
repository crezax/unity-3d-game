using UnityEngine;
using System.Collections.Generic;

public sealed class Spell {
	private PrefabsStorage prefabStorage;
	// Stuff that determines the range/area of spell
	public AreaOfEffect areaOfEffect;
	public float areaModifier;
	public float rangeModifier;
	
	// Stuff that determines casting/channeling speed
	public CastingType castingType;
	public float castingTime;
	
	// Methods invoked when spell is cast
	public List<SparkPowerPair> spellCasterEffects;
	public List<SparkPowerPair> spellTargetEffects;
	
	public Spell() {
		prefabStorage = GameObject.Find("_GameMaster").GetComponent<PrefabsStorage>();
	}
	
	// Actually casting spell
	public void Cast(SparkUser caster, BaseCharacter target) {
		switch(areaOfEffect) {
		case AreaOfEffect.SingleTarget:
			GameObject projectile = GameObject.Instantiate(prefabStorage.targetProjectilePrefab, caster.spellSpawnPoint.transform.position, Quaternion.identity) as GameObject;
			TargetProjectile projectileScript = projectile.GetComponent<TargetProjectile>();
			projectileScript.Setup(spellTargetEffects, target);
			break;
		case AreaOfEffect.TargetArea:
			GameObject spell = GameObject.Instantiate(prefabStorage.targetAreaSpellPrefab, GameMaster.GetMousePosition(), Quaternion.identity) as GameObject;
			spell.transform.localScale = new Vector3(areaModifier, areaModifier, areaModifier);
			TargetAreaSpell spellScript = spell.GetComponent<TargetAreaSpell>();
			spellScript.Setup(caster, GameMaster.GetMousePosition(), areaModifier, spellTargetEffects);
			break;
		}	
	}
}

public enum AreaOfEffect {
	SingleTarget,
	Cone,
	SkillShot,
	NearbyTargets,
	TargetArea
}

public enum CastingType {
	Instant,
	Casting,
	Channeling,
	MovingChannel
}

public enum TargettingType {
	Friendly,
	Enemy,
	Any
}