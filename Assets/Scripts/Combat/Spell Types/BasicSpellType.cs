using UnityEngine;
using System.Collections.Generic;

abstract public class BasicSpellType : BasicBehaviour {
	private SparkUser _caster;
	private List<SparkPowerPair> _effects;
	
	public SparkUser Caster {
		get { return _caster; }
		set { _caster = value; }
	}
	
	public List<SparkPowerPair> Effects {
		get { return _effects; }
		set { _effects = value; }
	}
}
