using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetAreaSpell : MonoBehaviour {
	public Transform myTransform;
	public SparkUser _caster;
	private float _radius;
	private List<SparkPowerPair> _effects;
	private Vector3 _center;
	
#region setters/getters
	public List<SparkPowerPair> Effects {
		get { return _effects; }
		set { _effects = value; }
	}
#endregion
	
	void Awake() {
		myTransform = transform;
		this.enabled = false;
	}

	// Use this for initialization
	void Start () {
		Collider[] hitColliders = Physics.OverlapSphere(this._center, this._radius);
		List<BaseCharacter> targets = new List<BaseCharacter>();
		foreach(Collider hitCollider in hitColliders) {
			if (hitCollider.GetComponent<BaseCharacter>() != null) {
				targets.Add(hitCollider.GetComponent<BaseCharacter>());
			}
		}
		foreach (SparkPowerPair r in _effects) {
			r.spark.Use(_caster, targets, _center, r.power);
		}
		Die();
	}
	
	public void Setup(SparkUser caster, Vector3 center, float radius, List<SparkPowerPair> effects) {
		this._caster = caster;
		this._effects = effects;
		this._radius = radius;
		this._center = center;
		this.enabled = true;
	}
	
	private void Die() {
		Destroy(gameObject, 5);	
	}
}
