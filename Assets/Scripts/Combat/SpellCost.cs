using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class SpellCost {
	private List<VitalValue> _costs;
	
	public SpellCost() {
		_costs = new List<VitalValue>();	
	}
	
	public void AddCost(VitalName vn, int val) {
		_costs.Add(new VitalValue(vn, val));
	}
	
	public ReadOnlyCollection<VitalValue> GetCosts() {
		return new ReadOnlyCollection<VitalValue>(_costs);
	}
}

public struct VitalValue {
	public VitalName vital;
	public int val;
	
	public VitalValue(VitalName vital, int val) {
		this.vital = vital;
		this.val = val;
	}
}