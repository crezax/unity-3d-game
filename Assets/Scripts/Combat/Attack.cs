using UnityEngine;
using System.Collections;

public abstract class Attack {
	
	public abstract void Perform(GameObject attacker, GameObject target);
	public abstract TargetLocation IsReachable(Transform attacker, Transform target);
	public abstract bool IsReady();
}

public enum TargetLocation {
	Reachable,
	OutOfRange,
	NotInLineOfSight
}