using UnityEngine;
using System.Collections.Generic;

namespace sparks.blue {
	public class Blink : Spark {
		private Timer _cooldown;
		
		public Blink() {
			_cooldown = new Timer(10);
			_sparkTarget = SparkTarget.Self;
		}
		
		public override string Name() {
			return "Blink";	
		}
		
		public override string Description() {
			return "Teleports your character forward";	
		}
		
		public override SpellCost Cost(int power) {
			//TODO: Make it make sense
			return null;	
		}
		
		protected override void OnUse(SparkUser caster, List<BaseCharacter> targets, Vector3 area, int power) {
			_cooldown.Start();
			// TODO: Make it actually move player
		}
		
		public override float Cooldown() {
			return _cooldown.Current();
		}
		
		public override bool IsUsable(BaseCharacter target, Transform area, int power) {
			return _cooldown.Current() == 0;
		}
	}
}
