using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace sparks.fire {
	public class FireDamage : Spark {
		public override string Name ()
		{
			throw new System.NotImplementedException ();
		}

		public override string Description ()
		{
			throw new System.NotImplementedException ();
		}

		public override SpellCost Cost (int power)
		{
			throw new System.NotImplementedException ();
		}

		protected override void OnUse (SparkUser caster, List<BaseCharacter> targets, Vector3 area, int power) {
			foreach (BaseCharacter target in targets) {
				target.ApplyDamage(new Damage(new MinMaxInterval<int>(1,3), DamageType.Fire));
			}
		}

		public override float Cooldown ()
		{
			throw new System.NotImplementedException ();
		}

		public override bool IsUsable (BaseCharacter target, Transform area, int power)
		{
			throw new System.NotImplementedException ();
		}
	}
}
