using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (CharacterAnimator))]
[RequireComponent (typeof (Mob))]
public class MobController : MonoBehaviour {
	
	private Mob _mob;
	private CharacterAnimator _mobAnimator;
	private MoveableMob _moveableMob;
	private LinkedList<Command> _commandQueue;
	private Transform _myTransform;
	
	public float MovementSpeed {
		get {
			return _moveableMob.MovementSpeed;	
		}
		set {
			_moveableMob.MovementSpeed = value;	
		}
	}
	
	public float RotationSpeed {
		get {
			return _moveableMob.RotationSpeed;	
		}
		set {
			_moveableMob.RotationSpeed = value;	
		}
	}
	
	void Awake() {
		_mob = GetComponent<Mob>();
		_mobAnimator = GetComponent<CharacterAnimator>();
		_moveableMob = GetComponent<MoveableMob>();
		_commandQueue = new LinkedList<Command>();
		_myTransform = transform;
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (_mob.IsAlive() == false) {
			_mobAnimator.PlayDie();
			_moveableMob.Stop();
			//_commandQueue.Clear();
			enabled = false;
			return;
		}
		if (_commandQueue.Count == 0) return;
		Debug.Log (_commandQueue.First.Value.type);
		switch (_commandQueue.First.Value.type) {
		
		case CommandType.Move:
			MoveCommand moveCommand = (MoveCommand)_commandQueue.First.Value;
			_commandQueue.RemoveFirst();
			_commandQueue.AddFirst(new MoveInProgressCommand(moveCommand.target));
			_mobAnimator.PlayRun();
			_moveableMob.Move(moveCommand.target);
			break;
			
		case CommandType.Attack:
			AttackCommand attackCommand = (AttackCommand)_commandQueue.First.Value;
			if (attackCommand.target.gameObject.GetComponent<BaseCharacter>().IsAlive() == false) {
				_commandQueue.RemoveFirst();
			}else if (attackCommand.attack.IsReachable(_myTransform, attackCommand.target) == TargetLocation.Reachable) {
				if (attackCommand.attack.IsReady()) {
					PerformAttack(attackCommand.attack, gameObject, attackCommand.target.gameObject);
				}
			} else {
				_commandQueue.AddFirst(new GetInRangeCommand(attackCommand.target, attackCommand.attack));
				_mobAnimator.PlayRun();
				_moveableMob.MoveTowards(attackCommand.target);
			}
			break;
			
		case CommandType.MoveInProgress:
			if (_myTransform.Equals(_commandQueue.First.Value.target)) {
				_mobAnimator.PlayIdle();
				_commandQueue.RemoveFirst();
			}
			break;
			
		case CommandType.GetInRange:
			GetInRangeCommand girCommand = (GetInRangeCommand)_commandQueue.First.Value;
			if (girCommand.attack.IsReachable(_myTransform, girCommand.target) == TargetLocation.Reachable) {
				_mobAnimator.PlayIdle();
				_moveableMob.RotateTowards(girCommand.target);
				_commandQueue.RemoveFirst();
				_commandQueue.AddFirst(new AttackCommand(girCommand.target, girCommand.attack));
			}
			break;
			
		case CommandType.Watch:
			WatchCommand watchCommand = (WatchCommand) _commandQueue.First.Value;
			
			_mobAnimator.PlaySteady();
			_moveableMob.RotateTowards(watchCommand.target);
			
			break;
		}
		
		if (_commandQueue.Count == 0) {
			Stop();	
		}
	}
	
	public void Move(Transform target) {
		Stop();
		QueueMove(target);
	}
	
	public void Stop() {
		_commandQueue.Clear();
		Debug.Log ("PLAYING IDLE");
		_mobAnimator.PlayIdle();
		_moveableMob.Stop();
	}
	
	public void Dance() {
		_mobAnimator.PlayDance();	
	}
	
	public void QueueMove(Transform target) {
		_commandQueue.AddLast(new MoveCommand(target));
	}
	
	public void Attack(Attack attack, GameObject target) {
		if (target.GetComponent<BaseCharacter>() == null) throw new MissingComponentException("TARGET IS NOT A CHARACTER");
		Stop();
		_commandQueue.AddLast(new AttackCommand(target.transform, attack));	
	}
	
	public void Watch(GameObject target) {
		Stop();
		_commandQueue.AddLast(new WatchCommand(target.transform));	
	}
	
	public bool IsFighting() {
		return _commandQueue.Count > 0 && (_commandQueue.First.Value.type == CommandType.Attack || _commandQueue.First.Value.type == CommandType.GetInRange);	
	}
	
	private void PerformAttack(Attack attack, GameObject attacker, GameObject target) {
		attack.Perform(attacker, target);
	}
}

enum CommandType {
	Move,
	Attack,
	MoveInProgress,
	GetInRange,
	Watch,
	KeepWatching
}
		
abstract class Command {
	public CommandType type;
	public Transform target;
}

class MoveCommand : Command {
	public MoveCommand(Transform target) {
		this.type = CommandType.Move;
		this.target = target;
	}
}

class AttackCommand : Command {
	public Attack attack;
	
	public AttackCommand(Transform target, Attack attack) {
		this.type = CommandType.Attack;
		this.target = target;
		this.attack = attack;
	}
}

class MoveInProgressCommand : Command {
	public MoveInProgressCommand(Transform target) {
		this.type = CommandType.MoveInProgress;
		this.target = target;
	}
}

class GetInRangeCommand : Command {
	public Attack attack;
	
	public GetInRangeCommand(Transform target, Attack attack) {
		this.type = CommandType.GetInRange;
		this.target = target;
		this.attack = attack;
	}
}

class WatchCommand : Command {
	public WatchCommand(Transform target) {
		this.type = CommandType.Watch;
		this.target = target;
	}
}

class KeepWatchingCommand : Command {
	public KeepWatchingCommand(Transform target) {
		this.type = CommandType.KeepWatching;
		this.target = target;
	}
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       