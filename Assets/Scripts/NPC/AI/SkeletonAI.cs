using UnityEngine;
using System.Collections;

[RequireComponent(typeof (MobController))]
[RequireComponent(typeof (MoveableMob))]
public class SkeletonAI : MonoBehaviour {
	
	private GameObject[] _players;
	private bool[] _watching;
	private BaseCharacter[] _baseCharacters;
	private MobController _mobController;
	protected float _detectionRange;
	protected float _attackRange;
	protected Attack _attack;
	protected Weapon _weapon;
	
	// Use this for initialization
	void Start () {
		InitArrays();
		_mobController = GetComponent<MobController>();
		_mobController.MovementSpeed = 1;
		_mobController.RotationSpeed = 1;
		_detectionRange = 20;
		_attackRange = 10;
		_weapon = WeaponsPackage.BasicSword;
		_attack = new Melee(_weapon);
	}
	
	// Update is called once per frame
	void Update () {
		if (_mobController.enabled == false) enabled = false;
		if (_players.Length == 0) {
			InitArrays();
		}
		if (_mobController.IsFighting()) return;
		float dist = _detectionRange;
		int closest = -1;
		for (int i = 0; i < _players.Length; i++) {
			if (_baseCharacters[i].IsAlive() == false) continue;
			if (closest == -1 || Vector3.Distance(transform.position, _players[i].transform.position) < dist) {
				dist = Vector3.Distance(transform.position, _players[i].transform.position);
				closest = i;
			}
		}
		
		if (closest == -1) {
			_mobController.Dance();
			return;
		}
		
		//Debug.Log(closest + " " + dist);
		
		if (dist < _detectionRange && !_watching[closest]) {
			_mobController.Watch(_players[closest]);
			_watching[closest] = true;
		}
		if (dist < _attackRange) {
			_mobController.Attack(_attack, _players[closest]);
			_watching[closest] = false;
			Debug.Log("[SKELETON AI] PLAYER " + _players[closest] + " " + dist);
		}
	}
	
	private void InitArrays() {
		_players = GameObject.FindGameObjectsWithTag("Player");
		_watching = new bool[_players.Length];
		_baseCharacters = new BaseCharacter[_players.Length];
		for (int i = 0; i < _players.Length; i++) {
			_baseCharacters[i] = _players[i].GetComponent<BaseCharacter>();	
		}
	}
}
